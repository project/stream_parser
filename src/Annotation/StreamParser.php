<?php
/**
 * @file
 * Contains \Drupal\stream_parser\Annotation\Parser.
 */

namespace Drupal\stream_parser\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a parser item annotation object.
 *
 * @see \Drupal\stream_parser\ParserPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class StreamParser extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the parser.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * The Plugin Options.
   *
   * @var array $options
   */
  public $options;

}
