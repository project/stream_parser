<?php

namespace Drupal\stream_parser;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface StreamParserInterface
 *
 * @package Drupal\stream_parser
 */
interface StreamParserInterface extends PluginInspectionInterface {

  /**
   * @return mixed
   */
  function getName();

  /**
   * @param string $data
   *
   * @return mixed
   */
  function prepare(string $data);

  /**
   * @return mixed
   */
  function write();

  /**
   * @param string $uri
   *
   * @return mixed
   */
  function getData(string $uri);

  /**
   * @return mixed
   */
  function getOptions();

  /**
   * @param $path
   *
   * @return mixed
   */
  function fetch($path);

}
