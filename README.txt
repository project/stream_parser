Provides parser plugins


Exemples :

$manager = \Drupal::service('plugin.manager.stream_parser');

/* CSV */

$instance = $manager->createInstance('csv');

// Load file.
$file = $this->entityTypeManager->getStorage('file')->load(1046);
    
// Get file uri.
$uri = $file->getFileUri();
$parsedCsvArray = $instance->getData($uri);


/* XML */

$instance = $manager->createInstance('xml');

// Load file.
$file = $this->entityTypeManager->getStorage('file')->load(1047);

// Get file uri.
$uri = $file->getFileUri();
$parsedXmlArray = $instance->getData($uri);

/* RSS */

$instance = $manager->createInstance('rss');
$parsedRssArray = $instance->getData('https://madeinfoot.ouest-france.fr/flux/rss_ligue.php?id=16');